#GENERIC N class Naive Bayes Classifier
#author: Harshvardhan Manjunatha    
#Always make code as short and sweet as possible


import sys, os, re
from math import log10

def main():

    inFile1 = sys.argv[1]    #model file
    inFile2 = sys.argv[2]   #dev/test file

    modelFile = open(inFile1, 'r')
    testFile = open(inFile2, 'r')

    probClassDict = {}
    priorsDict = {}

    i=0;

    noClasses = int(modelFile.readline())

    for i in range(noClasses):
        priorLine = modelFile.readline()
        words = priorLine.split()
        readClass = words.pop(0)
        priorsDict[readClass] = float(words[0])

   
    for eachLine in modelFile:
        words = eachLine.split()
        readClass = words.pop(0)
       
        if readClass in probClassDict.keys():
            probClassDict[readClass][words[0]] = float(words[1])

        else:
            probClassDict[readClass] = {}
            probClassDict[readClass][words[0]] = float(words[1])


    

    for eachLine in testFile:

        totProbDoc = {}     #classify per document, reset after each line

        words = eachLine.split()

        #adding/initializing priors
        for eachClass in probClassDict.keys():
            totProbDoc[eachClass] = priorsDict[eachClass]


        for wd in words:

            for eachClass in probClassDict.keys():

                if wd in probClassDict[eachClass].keys():
                    totProbDoc[eachClass] += probClassDict[eachClass][wd]
                else:
                    totProbDoc[eachClass] += probClassDict[eachClass]['unknownwordsymbol']

        

        print(max(totProbDoc, key=totProbDoc.get))

 
            

if __name__ == "__main__":
    main()
