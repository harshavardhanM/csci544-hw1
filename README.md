1.)
Feature Extractor Python Script Instructions and Examples

arg1 - path to directory containing Training/Development/Test files

arg2 - path to the generated TRAINING/DEV/TESTFILE with its filename

arg3 - "trainingOption" for Training, "otherOption" for generating DEVELOPMENT/TEST file


Eg: python3 fileExtractor.py /home/hm/Documents/csci544-hw1/SPAM_training/ /home/hm/Documents/csci544-hw1/spam_training.txt trainingOption

    python3 fileExtractor.py ./SENTIMENT_training/ ./sentiment_training.txt otherOption

Eg: python3 fileExtractor.py ./SPAM_training/ ./spam_training.txt trainingOption




Everything was implemented according to the SPEC

SPAM/HAM Development DataSet, Generic N class Naive Bayes Classifier

SPAM Label
Precision = 0.9672131147540983
Recall = 0.9752066115702479
F-Score = 0.9711934156378601

HAM Label
Precision = 0.9909729187562688
Recall = 0.988
F-Score = 0.9894842263395093


SENTIMENT Development Set, Generic N class Naive Bayes Classifier

POS Label

Precision = 0.9252336448598131
Recall = 0.8905547226386806
F-Score = 0.907563025210084

NEG Label

Precision = 0.8945086705202312
Recall = 0.9280359820089955
F-Score = 0.9109639440765268


PART 3)

When only 10% of the training data is used to train the classifiers in part 1 and part2, there is insufficient training data for the model to generalize over, resulting in a lower precision, recall and F-Score value

There should be sufficient training data for the model to generalize over. 

Insufficient data will result in new words appearing in the Dev/Test Set (not present in the Training set) being classified as Unknown words. 