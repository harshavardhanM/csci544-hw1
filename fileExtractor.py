'''
Created on Jan 28, 2015

@author: harsha

arg1 - path to directory containing Training/Development/Test files

arg2 - path to the generated TRAINING/DEV/TESTFILE with its filename

arg3 - "trainingOption" for Training, "otherOption" for generating DEVELOPMENT/TEST file


Eg: python3 fileExtractor.py /home/hm/Documents/csci544-hw1/SPAM_training/ /home/hm/Documents/csci544-hw1/spam_training.txt trainingOption

    python3 fileExtractor.py ./SENTIMENT_training/ ./sentiment_training.txt otherOption

Eg: python3 fileExtractor.py ./SPAM_training/ ./spam_training.txt trainingOption

'''
#Always make code as short and sweet as possible

import sys, re, os

def main():

    directoryPath = sys.argv[1]

    filePath = sys.argv[2]

    scriptOption = sys.argv[3]


    outFile = open(filePath, 'w')
                
    allFilesinPath = os.listdir(directoryPath)
       

    for eachFile in sorted(allFilesinPath):

        fileNameParts = eachFile.split('.')

        if(scriptOption == 'trainingOption'):
            print(fileNameParts[0], file=outFile, end=' ')
        
        fullPath = directoryPath + eachFile

        inFile = open(fullPath, 'r', errors='ignore')
        
        scrapedText = inFile.read()

        #remove punctuation
        #@ [] <br > <br />

        scrape1 = re.sub(r'[:;?!]+', '', scrapedText)

        scrape2 = re.sub(r'[.,"]+', '', scrape1)
    
        cleanedDoc = re.sub(r"['()-]+", "", scrape2)
                
        listOfwords = cleanedDoc.split();

               
        for eachWord in listOfwords:
            print(eachWord.lower(), file=outFile, end = ' ')
        
        print(file=outFile) #print newline character
        
    
    
    
    
    ''' split on whitepsace '''
       

if __name__ == "__main__":
    main()