#GENERIC N class Naive Bayes Classifier
#author: Harshvardhan Manjunatha    
#Always make code as short and sweet as possible

import sys, re, os
from math import log10


# Unknown word symbol demarcated to all words that are not in spam/ham dictionaries

def main():
    
    inFile = sys.argv[1]    #training file
    outFile = sys.argv[2]   #model file

    inF = open(inFile, 'r')
    outF = open(outFile, 'w')

    classDict = {}
    docsFreq = {}
    totalDocs = 0

    for eachLineDoc in inF:      # once inF is used, all lines in file are read, inF become empty
        totalDocs += 1
        words = eachLineDoc.split()
        interpretedClass = words.pop(0)

               
        if interpretedClass in docsFreq.keys():
            docsFreq[interpretedClass] += 1
        else:
            docsFreq[interpretedClass] = 1
        

        if interpretedClass in classDict.keys():
            for wd in words:
                if wd in classDict[interpretedClass].keys():               
                    classDict[interpretedClass][wd] += 1
                else:
                    classDict[interpretedClass][wd] = 1
        else:
            classDict[interpretedClass] = {}
            for wd in words:
                if wd in classDict[interpretedClass].keys():
                    classDict[interpretedClass][wd] += 1
                else:
                    classDict[interpretedClass][wd] = 1
            
    
    # log(PRIORS)

    #print(totalDocs, file=outF)

    print(len(docsFreq.keys()), file=outF)

    for eachClass in docsFreq.keys():
        pvalue = docsFreq[eachClass]/totalDocs
        logp = log10(pvalue)
        #print(eachClass, str(docsFreq[eachClass]), str(pvalue), str(logp), file=outF)
        print(eachClass, str(logp), file=outF)

        
    totWordsClass = {}
    vocabDict = {}
    
    for eachClass in classDict.keys():
        totWordsClass[eachClass] = sum( classDict[eachClass].values() )
                                
        for eachWord in classDict[eachClass]:
            vocabDict[eachWord] = 1


    vocabSize = len(vocabDict.keys())

    for eachClass in classDict.keys():
        tempwordFreqdict = classDict.get(eachClass)
        #print(eachClass, file=outF)
     
        unknownVal = 1/(totWordsClass[eachClass] + vocabSize)
        logUnknown = log10(unknownVal)
        print(eachClass, 'unknownwordsymbol', str(logUnknown), file=outF)

        #log(add one smoothing)
        for eachword in tempwordFreqdict:
            partValue = (tempwordFreqdict[eachword] + 1)/(totWordsClass[eachClass] + vocabSize)
            plogVal = log10(partValue)
            print(eachClass, eachword, str(plogVal), file=outF)



    
    #for k in vocabDict.keys():
      #  print(k, ':', vocabDict[k], file=outF, end = ' ')

    #for l in classDict.keys():
     #   print(l, '->', classDict[l], file=outF)
         
    

if __name__ == "__main__":
	main()
